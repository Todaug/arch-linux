# Packages

* sudo pacman -S i3blocks rofi rxvt-unicode sysstat pulsemixer feh

# Previews

Desktop
![Desktop](https://gitlab.com/Todaug/arch-linux/raw/master/previews/desktop.png)

Rofi
![Rofi](https://gitlab.com/Todaug/arch-linux/raw/master/previews/rofi.png)

i3
![Rofi](https://gitlab.com/Todaug/arch-linux/raw/master/previews/i3.png)


