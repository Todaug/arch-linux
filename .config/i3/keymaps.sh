#!/bin/bash

current_layout=$(setxkbmap -query | awk -F ':' '/layout/{print $2}' | tr -d '[:space:]')

if [ "$current_layout" == "ca" ]; then
    setxkbmap us
else
    setxkbmap ca -variant multix
fi
